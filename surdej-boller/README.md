# Hurtige surdejs-boller med god smag

[<img alt="alt_text" width="400px" src="PXL_20230503_183808924.jpg" />](https://www.google.com/)

## Ingredienser

- 550 gram lunkent vand
- 15 gram frisk gær
- 100 gram "aktiv" surdej
- 600 gram bage-hvedemel min. 12% protein
- 100 gram rugmel
- 15 gram salt

## Fremgangsmåde

1. Rør vand, frisk gær og aktiv surdej sammen til det er opløst.
2. Kom hvedemel og rugmel i ælt det sammen med en røremaksine i ca. 5 min.
3. Kom salt i og ælt det yderligere 5 min.
4. Lad dejen hæve i 1.5 - 2 timer til den er hævet til dobbelt størrelse
---
5. Drys godt med mel på bordet og skrab forsigtigt dejen ud på melen. ( der skal bruges en del mel, da dejen er meget blød )
6. Fold forsigtigt dejen indover sig selv og del den op i to lige store aflange klumper
7. Udstik boller i passende størrelser og placer dem på en bageplade
8. Tænd ovn på 250 grader varmluft - Lad bollerne efterhæve med et viskestykke over mens ovnen bliver varm.
9. Bag bollerne ca. 17 min. til de er flotte brune i farven.
